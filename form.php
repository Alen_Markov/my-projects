<!DOCTYPE html>
<html>
    <head>
        <title>Document</title>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />

        <!-- Latest compiled and minified Bootstrap 4.6 CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <!-- CSS script -->
        <link rel="stylesheet" href="style.css">
        <!-- Latest Font-Awesome CDN -->
        <script src="https://kit.fontawesome.com/64087b922b.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-warning">
     <!-- NAVBAR  -->
     <nav class="navbar navbar-expand-lg navbar-light box-shadow">
        <a class="navbar-brand text-center ml-lg-5 " href="./index.html"><img src="./Logo.png" alt="Logo" width="50px"> <br> <span class="font-size-xs">BRAINSTER</span></a>
        <button class="navbar-toggler" type="button" data-toggle="modal" data-target="#Modalnav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="fa-solid fa-bars-staggered fa-2x"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item margin-left-navbar-item-lg">
                    <a class="nav-link mr-2" href="https://brainster.co/marketing/">Академија за маркетинг</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mr-2" href="https://brainster.co/full-stack/">Академија за програмирање</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link mr-2" href="https://brainster.co/data-science/">Академија за data science</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link mr-2 " href="https://brainster.co/graphic-design/">Академија за дизајн</a>
                </li>
            </ul>
            <p class="navbar-text mb-0 mr-5"><button type="button" class="btn btn-danger">Вработи наш студент</button></p>
        </div>
    </nav>
    <!-- NAVBAR  -->
<!-- FORM  -->
<h1 class="text-center py-3 mt-5 mb-5 font-size-xl section-control-form">Вработи студенти</h1>

<div class="container-fluid section-control-bottom">
    <form action="connect.php" method="post">
        <div class="form-row justify-content-center">
          <div class="form-group col-lg-4">
            <label for="inputtext" class="col-form-label-lg font-weight-bold">Име и презиме</label>
            <input type="text" class="form-control form-control-lg" id="inputEmail4" placeholder="Вашето име и презиме" name="FullName">
          </div>
          <div class="form-group col-lg-4">
            <label for="inputtext" class="col-form-label-lg font-weight-bold">Име на компанија</label>
            <input type="text" class="form-control form-control-lg" id="inputtext" placeholder="Име на вашата компанија" name="CompanyName">
          </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-lg-4">
              <label for="inputEmail4" class="col-form-label-lg font-weight-bold">Контакт имејл</label>
              <input type="email" class="form-control form-control-lg" id="inputEmail4" placeholder="Контакт имејл на вашата компанија" name="Email">
            </div>
            <div class="form-group col-lg-4">
              <label for="inputtext" class="col-form-label-lg font-weight-bold">Контакт телефон</label>
              <input type="number" class="form-control form-control-lg" id="inputtext" placeholder="Контакт телефон на вашата компанија" name="Phone">
            </div>
          </div>

          <div class="form-row justify-content-center">
            <div class="form-group col-lg-4">
                <label for="inputSelect" class="col-form-label-lg font-weight-bold">Тип на студенти</label>
                  <select class="form-control form-control-lg" id="inputSelect" name="TypeStudents">
                  <option value="" disabled selected class='d-none'>Изберете тип на студент</option>
                  <?php
include 'typeofstudent.php';
?>
                  </select>
            </div>
            <div class="form-group col-lg-4">
                <input type="submit" class="btn btn-danger btn-block btn-lg margin-button font-weight-bold"value="ИСПРАТИ">
          </div>
    </form>
</div>
</div>

<!-- FOOTER  -->
<div class="container-fluid bg-dark text-white text-center py-2">
    <div class="row">
        <div class="col-12">
            <p class="mb-0">
                Изработено со <i class="fa-solid fa-heart text-danger"></i> од студентите на Brainster
            </p>
        </div>
    </div>
</div>



<!-- MODALS  -->
<div class="modal bg-dark text-white" id="Modalnav">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header bg-dark">
            <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <!-- Modal Body -->
        <div class="modal-body bg-dark bolder">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#">Академија за маркетинг </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="#">Академија за програмирање</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="#">Академија за data science </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="#">Академија за дизајн</a>
                </li>
              
                
                <li class="mt-3"><button class="btn btn-danger my-2 mx-lg-5 my-sm-0" type="submit">
                    Вработи наш студент
                </button>
            </li>
            </ul>
              
        </div>



        <!-- jQuery library -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="ha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        
        <!-- Latest Compiled Bootstrap 4.6 JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    </body>
</html>