<?php
$conn = new mysqli('localhost','root','D3stboy3b','Project1');
if($conn->connect_error){
    die('Connection Failed : '.$conn->connect_error);}
$sql = 'SELECT
registration.FullName,
registration.CompanyName,
registration.Email,
registration.Phone,
typeofstudents.TypeOfStudents as StudentsType
FROM registration
left outer JOIN typeofstudents ON registration.TypeStudents = typeofstudents.id
' ;
$result = mysqli_query($conn,$sql);
$clients = mysqli_fetch_all($result, MYSQLI_ASSOC);
mysqli_free_result($result);
mysqli_close($conn);



?>

!<!DOCTYPE html>
<html>
    <head>
        <title>Document</title>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />

        <!-- Latest compiled and minified Bootstrap 4.6 CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <!-- CSS script -->
        <link rel="stylesheet" href="style.css">
        <!-- Latest Font-Awesome CDN -->
        <script src="https://kit.fontawesome.com/64087b922b.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <h1 class=text-center>Clients</h1>
        <div class="container">
<div class="row">
    <?php
foreach($clients as $client){?>
<div class="col-12">
<table class="table table-striped table-dark">
<thead>
    <tr>
      <th scope="col">Име и презиме</th>
      <th scope="col">Име на компанија</th>
      <th scope="col">Контакт имејл</th>
      <th scope="col">Контакт телефон</th>
      <th scope="col">Тип на студенти</th>

    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center"><?php echo htmlspecialchars ($client['FullName']); ?></td>
      <td><?php echo htmlspecialchars ($client['CompanyName']); ?></td>
      <td><?php echo htmlspecialchars ($client['Email']); ?></td>
      <td><?php echo htmlspecialchars ($client['Phone']); ?></td>
      <td><?php echo htmlspecialchars ($client['StudentsType']); ?></td>
    </tr>
  </tbody>
</table>
</div>

    <?php  }?>
</div>
        </div>





        <!-- jQuery library -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="ha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        
        <!-- Latest Compiled Bootstrap 4.6 JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    </body>
</html>